---
title: "About"
description: "MyHops is all about Innovation and Cloud Native Technologies"
featured_image: 'images/clouds-and-sea.png'
menu: 
  main:
    weight: 1000
---

My name is Peter Zandbergen and MyHops is the name of the company that I used for contracting work.
On March 7th 2022 I joined Ordina Netherlands as Senior Principal Expert - Platform & Cloud Architecture. 
Working for an IT company allows me to spend more time working on IT challenges.

I use this site to post IT related articles and information that have my personal 
professional interest, especially about Cloud Native technologies and methodologies.

You can find more information about me on my [LinkedIn profile page](https://www.linkedin.com/in/peterzandbergen/).