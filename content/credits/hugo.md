---
title: "Hugo"
date: 2022-01-12T21:06:52-05:00
# draft: true
weight: 1
disable_share: true
---

[Hugo](https://gohugo.io/) is a static site generator build in Go. It is fast, has a lot of ready to use themes
 and works well with CI/CD pipelines. And it is fast.

 <!--more-->