---
title: "Gitlab"
date: 2022-01-01T21:06:44-05:00
# draft: true
weight: 2
type: page
disable_share: true

---

I use [GitLab](https://about.gitlab.com) to store and the sources for this site. 

[GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) 
builds the site after each push and the 
generated static files are served from [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).

<!--more-->