---
title: "Go"
date: 2022-01-12T21:07:02-05:00
# draft: true
weight: 3
disable_share: true
---

I do not want to leave out [Go](https://go.dev/), the programming language
that is very popular with some major open source projects, 
like Kubernetes, Docker and Hugo. 

It is easy to learn, compiles really fast into easy to deploy
single binaries. The ideal language for Cloud Native application
development.

<!--more-->