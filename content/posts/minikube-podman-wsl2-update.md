---
title: "Minikube - podman - wsl2 - update"
date: 2022-04-09T17:14:03+02:00
draft: false
toc: true
disable_share: true
---

This is an update of the
[Minikube - podman - wsl2]({{< ref "/posts/minikube-podman-wsl2" >}}) post. The latest releases of Minikube and Podman make it a lot easier to run Kubernetes on WSL2. 

This tutorial is for people who are interested in using 
Kubernetes on a Windows 10+ PC or laptop. At the end of this post you should have a workable platform for developing for 
or exploring Kubernetes. 

<!--more-->

## Minikube and Podman Improvements

Minikube has improved the support of Podman to run the 
Kubernetes cluster in. It now uses the Podman driver as 
default on WSL2. It now also supports different container
runtimes, docker, containerd and crio.

The improved Podman support eliminates the need for a docker
client, which was needed to get the ```minikube tunnel``` 
command working. 

## Preparation

First you need to make sure that you have podman working on WSL2. And you need to install some additional tools
to work with Kubernetes. 

You find the links for the tools you need to install below. 

- [minikube](https://minikube.sigs.k8s.io/docs/start/)
- [podman](https://podman.io/getting-started/installation)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)

## Starting Minikube with podman

The [minikube manual](https://minikube.sigs.k8s.io/docs/drivers/podman/) 
recommends to use the cri-o in combination with the podman driver, but this runtime does not work well with the 
**metallb** load balancer. Use **containerd** instead.

Start the cluster with the following command. 
If you want you can add the --profile option
to create a cluster with a different name.


NOTE: If you get an error message about insufficient rights,
follow the instructions to add your account to the 
sudoers list. In my case I had to create a file with 
the follwing content in the **/etc/sudoers.d** directory. 
Replace **peza** with your username.

```
peza ALL=(ALL) NOPASSWD: /usr/bin/podman
```

Start the cluster:

```
minikube start --driver=podman --container-runtime=containerd
```

```
😄  minikube v1.25.2 on Ubuntu 20.04
✨  Using the podman driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
E0409 17:44:48.698307  126646 cache.go:203] Error downloading kic artifacts:  not yet implemented, see issue #8426
🔥  Creating podman container (CPUs=2, Memory=2400MB) ...
🎁  Preparing Kubernetes v1.23.3 on CRI-O 1.22.1 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔗  Configuring CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Test if the cluster properly works 
with ```kubectl cluster-info```. You should get
a similar output as shown below.

```
$ kubectl cluster-info
Kubernetes control plane is running at https://127.0.0.1:36983
CoreDNS is running at https://127.0.0.1:36983/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

Test if you can run a pod in the cluster. I prefer to use the alpine image, because it is very versatile and lightweight
and it allows me to install additional packages.

```
$ kubectl run testpod -it --image=alpine
If you don't see a command prompt, try pressing enter.
/ #
```

When you get a simular response, you know that your cluster is
working properly.

## Enable traffic to your cluster

The current version of minikube does not need the workarounds
I described in [Minikube - podman - wsl2]({{< ref "/posts/minikube-podman-wsl2" >}}) to enable traffic to services on the cluster.

Just run the **minikube tunnel** command, leave it running and you're ready to accept ingress traffic on services of the 
type LoadBalancer.

```
$ minikube tunnel
✅  Tunnel successfully started

📌  NOTE: Please do not close this terminal as this process must stay alive for the tunnel to be accessible ...
```

## Test Minikube with the metallb LoadBalancer

With the tunnel running, I can access the services from outside the cluster.

Enable the loadbalancer that comes with minikube, 
metallb.

```
minikube addons enable metallb
```

Install a simple app with the following command that you can copy and paste into your terminal.

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Service
metadata:
  labels:
    app: myipaddress
  name: myipaddress
spec:
  ports:
  - port: 18080
    protocol: TCP
    targetPort: 8080
  selector:
    app: myipaddress
  type: LoadBalancer
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: myipaddress
  name: myipaddress
spec:
  containers:
  - image: docker.io/peterzandbergen/myipaddress
    name: myipaddress

EOF
```

The terminal that is running the **minikube tunnel** command
will show that it started tunnel for the new service **myipaddress**.

```
✅  Tunnel successfully started

📌  NOTE: Please do not close this terminal as this process must stay alive for the tunnel to be accessible ...

🏃  Starting tunnel for service myipaddress.
```

Use curl to test the service.

```
$ curl http://localhost:18080
Instance info:
    Instance:         myipaddress_1
    Server hostname:  myipaddress
    Open connections: 1
Request info:
    Host:                localhost:18080
    Method:              GET
    Request uri:         /
    Listen port:         8080
    Remote address:      10.244.0.1:15842
    UserAgent:           curl/7.68.0
Headers:
    Accept: */*
    User-Agent: curl/7.68.0
Body: size=0
```

## Conclusion

The new version of Minikube improved the support for
Podman and make it a lot easier to use Kubernetes 
in WSL2. 

## Used versions

WSL2 Linux
```
$ lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 20.04.4 LTS
Release:        20.04
Codename:       focal
```

Minikube
```
$ minikube version
minikube version: v1.25.2
commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7
```

Podman
```
$ podman version
Version:      3.4.2
API Version:  3.4.2
Go Version:   go1.16.6
Built:        Thu Jan  1 01:00:00 1970
OS/Arch:      linux/amd64
```