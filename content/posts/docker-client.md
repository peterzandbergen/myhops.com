---
title: "Docker Client"
date: 2022-01-05T16:31:46+01:00
draft: false
toc: true
disable_share: true
---

This is how you can install the docker client in WSL2. 
This is an supporting article for the post
[Minikube - podman - wsl2]({{< ref "/posts/minikube-podman-wsl2" >}})
that needs the docker client app as one of the prerequites.

<!--more-->

## Installation Steps

Download the correct tar file for your Linux system using the link below.

[docker static binary archive](https://download.docker.com/linux/static/stable/)

Unpack the tar file and copy the *docker/docker* file to 
a location in your path, e.g. /usr/local/bin.

```
tar -xzf docker-20.10.9.tgz
sudo cp docker/docker /usr/local/bin
```

Try to run the client

```
$ docker --help

Usage:  docker [OPTIONS] COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/home/peza/.docker")
  -c, --context string     Name of the context to use to connect to the daemon (overrides DOCKER_HOST env var and
                           default context set with "docker context use")
  -D, --debug              Enable debug mode

...
[extra text deleted]

```